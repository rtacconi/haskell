module FstPrime where

fst' :: (a, b) -> a
fst' (a, b) = a
snd' :: (a, b) -> b
snd' (a, b) = b

tupFunc :: (Integer, [a])
  -> (Integer, [a])
  -> (Integer, [a])
tupFunc (a, b) (c, d) =
  ((a + c), (b ++ d))
